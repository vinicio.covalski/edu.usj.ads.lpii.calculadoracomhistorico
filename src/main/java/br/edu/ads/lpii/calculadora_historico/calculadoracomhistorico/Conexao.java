package br.edu.ads.lpii.calculadora_historico.calculadoracomhistorico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Conexao {

	public static void main(String[] args) {
		SpringApplication.run(Conexao.class, args);
	}

}

package br.edu.ads.lpii.calculadora_historico.calculadoracomhistorico;

import java.util.ArrayList;
import java.util.List;

public class Historico {

    List<Operacao> listadeOperacoes = new ArrayList<>();

    void adicionar(Operacao operacaoresultado) {
        listadeOperacoes.add(operacaoresultado);
    }

    List<Operacao> getHistorico() {
        return listadeOperacoes;
    }
}

    

package br.edu.ads.lpii.calculadora_historico.calculadoracomhistorico;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@Controller //Define que minha classe será um controller - informa a resposta para o usuário, passando os atributos visiveis e recebendo os parametros vindos do view
public class Calculadora {
    
    Historico historico = new Historico();
    @Autowired
    OperacaoRepository operacaoRepository;

@PostMapping(value="/calcula")// @PostMappingOs métodos anotados manipulam as POSTsolicitações HTTP correspondentes à expressão de URI fornecida. nesse caso o index.html está utilizando o metodo post <form action="/calcula" method="POST"> este será chamado nessa função.
public ModelAndView postCalcula(@RequestParam String numero1, @RequestParam String numero2,@RequestParam String operador){
   
  
       
        Double resultado = 0.0;  // iniciando a varíavel com valor zerado
        Double n1 = Double.parseDouble(numero1); // Armazendo a variavel recebida do index.html
        Double n2 = Double.parseDouble(numero2); // Armazendo a variavel recebida do index.html
        String operacaoresultado = ""; 
        //  System.out.println("Passei por aqui");
     //  System.out.println(numero1);
     //  System.out.println(operador);
     //  System.out.println(numero2);
     //  System.out.println(n1);
     //  System.out.println(n2);


        switch(operador) {
           
//Utilizado o switch case para as operações da calculadora
            case "+" :
            resultado = n1 + n2;
            operacaoresultado = n1 +" + "+ n2 + " = "+ resultado.toString();
      //      System.out.println("etapa2");
            break;
            case "-" :
            resultado = n1 - n2;
            operacaoresultado = n1 +" - "+ n2 + " = "+ resultado.toString();
       //     System.out.println("etapa3");
            break;
            case "*" :
            resultado = n1 * n2;
            operacaoresultado = n1 +" * "+ n2 + " = "+   resultado.toString();
        //    System.out.println("etapa4");
            break;
            case "/" :
            resultado = n1 / n2;
            operacaoresultado = n1 +" / "+ n2 + " = "+  resultado.toString();
        //    System.out.println("etapa5");
            break;
            default:
         //   System.out.println("etapa6");
            resultado = 0.0;
            operacaoresultado = resultado.toString();

        }


        //System.out.println(resultado);
        Operacao operacao = new Operacao();


        operacao.setOperacao(operacaoresultado);
        operacaoRepository.save(operacao);
        historico.adicionar(operacao);
        ModelAndView modelAndView = new ModelAndView("index"); // Criando um construtor chamando o templates/index

        modelAndView.addObject("mensagem", operacaoresultado);
        modelAndView.addObject("historico", operacaoRepository.findAll());

       
      //  String texto = "O resultado é "+resultado;//Printa no navegador o resultado.

     //   modelAndView.addObject("mensagem", texto); // Atribuindo o atributo texto para retorno da função.
    return modelAndView;
}

@GetMapping(value="/deleta/{id}")
public ModelAndView getDeleta(@PathVariable Long id) {
    operacaoRepository.deleteById(id);
    ModelAndView modelAndView = new ModelAndView("index"); // Criando um construtor chamando o templates/index
    modelAndView.addObject("historico", operacaoRepository.findAll());
    return modelAndView;
}

@GetMapping(value="/mostra/{id}")
public ModelAndView getMostra(@PathVariable Long id) {
    Operacao operacao = null;
    String texto = "";
    System.out.println("1");
    if (operacaoRepository.findById(id).isPresent()){
        
        operacao = operacaoRepository.findById(id).get();
        System.out.println("2");
        texto = operacao.getId() + " : " +operacao.getOperacao();
        System.out.println(texto);

    }
    System.out.println("3");

    ModelAndView modelAndView = new ModelAndView("mostra_operacao"); // Criando um construtor chamando o templates/index
    modelAndView.addObject("texto", texto);
    return modelAndView;
}














}
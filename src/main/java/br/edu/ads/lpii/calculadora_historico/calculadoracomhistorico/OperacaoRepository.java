package br.edu.ads.lpii.calculadora_historico.calculadoracomhistorico;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface OperacaoRepository extends CrudRepository<Operacao,Long> {
  List<Operacao> findAll();

}
    

package br.edu.ads.lpii.calculadora_historico.calculadoracomhistorico;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
@Entity
    
public class Operacao {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    Long id;
    
    String operacao;

}